FROM centos:8

RUN yum install -y epel-release && \
    yum update -y && \
    yum clean all

RUN yum install -y gcc gcc-c++ \
                   libtool libtool-ltdl \
                   rsync \
                   make cmake \
                   git \
                   pkgconfig \
                   sudo gnupg2 \
                   automake autoconf \
                   yum-utils rpm-build \
		           rpm-sign expect createrepo && \
    yum clean all
